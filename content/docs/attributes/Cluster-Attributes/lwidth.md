---
title: lwidth
categories:  ["cluster"]
tags: ["tbd", "attribute", "cluster", "lwidth", "double"]
date: 2018-04-15

description: "Width of cluster label in inches."
types: ["double"]
default: "n/a"
min: ""
restrictions: "write only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Width of cluster label in inches.
