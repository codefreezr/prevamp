---
title: K
categories:  ["cluster"]
tags: ["tbd", "attribute", "cluster", "k", "double"]
date: 2018-04-15

description: "Spring constant used in virtual physical model."
types: ["double"]
default: "0.3"
min: "0"
restrictions: "sfdp, fdp only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Spring constant used in virtual physical model. It roughly corresponds to an ideal edge length (in inches), in that increasing K tends to increase the distance between nodes. Note that the edge attribute len can be used to override this value for adjacent nodes.
