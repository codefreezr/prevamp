---
title: bgcolor
categories:  ["cluster"]
tags: ["tbd", "attribute", "cluster", "bgcolor", "color", "colorlist"]
date: 2018-04-15

description: "Background for entire cluster."
types: ["color", "colorList"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Background for entire cluster.
