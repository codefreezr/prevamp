---
title: nojustify
categories:  ["cluster"]
tags: ["tbd", "attribute", "cluster", "nojustify", "bool"]
date: 2018-04-15

description: "If nojustify is 'true', multi-line labels will be justified in the context of itself."
types: ["bool"]
default: "FALSE"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If nojustify is 'true', multi-line labels will be justified in the context of itself.
