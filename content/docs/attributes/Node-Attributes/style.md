---
title: style
categories:  ["node"]
tags: ["tbd", "attribute", "node", "style", "style"]
date: 2018-04-15

description: "Set style information for components of the node."
types: ["style"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Set style information for components of the node.
