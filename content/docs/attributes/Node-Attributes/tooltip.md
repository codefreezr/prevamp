---
title: tooltip
categories:  ["node"]
tags: ["tbd", "attribute", "node", "tooltip", "escstring"]
date: 2018-04-15

description: "Tooltip annotation attached to the node."
types: ["escString"]
default: "n/a"
min: ""
restrictions: "svg, cmap only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Tooltip annotation attached to the node. If unset, Graphviz will use the object's label if defined. Note that if the label is a record specification or an HTML-like label, the resulting tooltip may be unhelpful. In this case, if tooltips will be generated, the user should set a tooltip attribute explicitly.
