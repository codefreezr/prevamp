---
title: layer
categories:  ["node"]
tags: ["tbd", "attribute", "node", "layer", "layerrange"]
date: 2018-04-15

description: "Specifies layers in which the node is present."
types: ["layerRange"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Specifies layers in which the node is present.
