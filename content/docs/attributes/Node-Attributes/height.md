---
title: height
categories:  ["node"]
tags: ["tbd", "attribute", "node", "height", "double"]
date: 2018-04-15

description: "Height of node, in inches."
types: ["double"]
default: "0.5"
min: "0.02"
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Height of node, in inches. This is taken as the initial, minimum height of the node. If fixedsize is true, this will be the final height of the node. Otherwise, if the node label requires more height to fit, the node's height will be increased to contain the label. Note also that, if the output format is dot, the value given to height will be the final value.
