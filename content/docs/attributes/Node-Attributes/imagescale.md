---
title: imagescale
categories:  ["node"]
tags: ["tbd", "attribute", "node", "imagescale", "bool", "string"]
date: 2018-04-15

description: "Attribute controlling how an image fills its containing node."
types: ["bool", "string"]
default: "FALSE"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Attribute controlling how an image fills its containing node. In general, the image is given its natural size, (cf. dpi), and the node size is made large enough to contain its image, its label, its margin, and its peripheries. Its width and height will also be at least as large as its minimum width and height. If, however, fixedsize=true, the width and height attributes specify the exact size of the node.
