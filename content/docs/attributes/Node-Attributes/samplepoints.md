---
title: samplepoints
categories:  ["node"]
tags: ["tbd", "attribute", "node", "samplepoints", "int"]
date: 2018-04-15

description: "If the input graph defines the vertices attribute, and output is dot or xdot, this gives the number of points used for a node whose shape is a circle or ellipse."
types: ["int"]
default: "8(output), 20(overlap and image maps)"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If the input graph defines the vertices attribute, and output is dot or xdot, this gives the number of points used for a node whose shape is a circle or ellipse. It plays the same role in neato, when adjusting the layout to avoid overlapping nodes, and in image maps.
