---
title: fontsize
categories:  ["node"]
tags: ["tbd", "attribute", "node", "fontsize", "double"]
date: 2018-04-15

description: "Font size, in points, used for text."
types: ["double"]
default: "14.0"
min: "1.0"
restrictions: ""
examples: "crazy.gv"
lastEdit: 2018-04-15
lastEditor: generator
---

Font size, in points, used for text.
