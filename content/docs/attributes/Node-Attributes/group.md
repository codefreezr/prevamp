---
title: group
categories:  ["node"]
tags: ["tbd", "attribute", "node", "group", "string"]
date: 2018-04-15

description: "If the end points of an edge belong to the same group, i."
types: ["string"]
default: "n/a"
min: ""
restrictions: "dot only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If the end points of an edge belong to the same group, i.e., have the same group attribute, parameters are set to avoid crossings and keep the edges straight.
