---
title: fixedsize
categories:  ["node"]
tags: ["tbd", "attribute", "node", "fixedsize", "bool", "string"]
date: 2018-04-15

description: "If false, the size of a node is determined by smallest width and height needed to contain its label and image, if any, with a margin specified by the margin attribute."
types: ["bool", "string"]
default: "FALSE"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If false, the size of a node is determined by smallest width and height needed to contain its label and image, if any, with a margin specified by the margin attribute. The width and height must also be at least as large as the sizes specified by the width and height attributes, which specify the minimum values for these parameters.
