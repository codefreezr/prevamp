---
title: pin
categories:  ["node"]
tags: ["tbd", "attribute", "node", "pin", "bool"]
date: 2018-04-15

description: "If true and the node has a pos attribute on input, neato or fdp prevents the node from moving from the input position."
types: ["bool"]
default: "FALSE"
min: ""
restrictions: "fdp, neato only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If true and the node has a pos attribute on input, neato or fdp prevents the node from moving from the input position. This property can also be specified in the pos attribute itself (cf. the point type).
