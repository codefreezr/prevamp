---
title: layers
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "layers", "layerlist"]
date: 2018-04-15

description: "Specifies a linearly ordered list of layer names attached to the graph The graph is then output in separate layers."
types: ["layerList"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Specifies a linearly ordered list of layer names attached to the graph The graph is then output in separate layers. Only those components belonging to the current output layer appear. For more information, see the page How to use drawing layers (overlays).
