---
title: lp
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "lp", "point"]
date: 2018-04-15

description: "Label position, in points."
types: ["point"]
default: "n/a"
min: ""
restrictions: "write only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Label position, in points. The position indicates the center of the label.
