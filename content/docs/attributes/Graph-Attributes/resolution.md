---
title: resolution
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "resolution", "double"]
date: 2018-04-15

description: "This is a synonym for the dpi attribute."
types: ["double"]
default: "96.0, 0.0"
min: ""
restrictions: "svg, bitmap output only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

This is a synonym for the dpi attribute.
