---
title: charset
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "charset", "string"]
date: 2018-04-15

description: "Specifies the character encoding used when interpreting string input as a text label."
types: ["string"]
default: "UTF-8"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Specifies the character encoding used when interpreting string input as a text label.
