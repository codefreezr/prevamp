---
title: ratio
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "ratio", "double", "string"]
date: 2018-04-15

description: "Sets the aspect ratio (drawing height/drawing width) for the drawing."
types: ["double", "string"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Sets the aspect ratio (drawing height/drawing width) for the drawing.
