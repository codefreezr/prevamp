---
title: quantum
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "quantum", "double"]
date: 2018-04-15

description: "If quantum > 0."
types: ["double"]
default: "0.0"
min: "0.0"
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If quantum > 0.0, node label dimensions will be rounded to integral multiples of the quantum.
