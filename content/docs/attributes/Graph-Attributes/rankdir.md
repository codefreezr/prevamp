---
title: rankdir
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "rankdir", "rankdir"]
date: 2018-04-15

description: "Sets direction of graph layout."
types: ["rankdir"]
default: "TB"
min: ""
restrictions: "dot only"
examples: "fsm.gv"
lastEdit: 2018-04-15
lastEditor: generator
---

Sets direction of graph layout. For example, if rankdir='LR', and barring cycles, an edge T -> H; will go from left to right. By default, graphs are laid out from top to bottom.
