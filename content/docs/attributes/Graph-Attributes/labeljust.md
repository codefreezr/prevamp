---
title: labeljust
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "labeljust", "string"]
date: 2018-04-15

description: "Justification for cluster labels."
types: ["string"]
default: "c"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Justification for cluster labels. If 'r', the label is right-justified within bounding rectangle; if 'l', left-justified; else the label is centered. Note that a subgraph inherits attributes from its parent. Thus, if the root graph sets labeljust to 'l', the subgraph inherits this value.
