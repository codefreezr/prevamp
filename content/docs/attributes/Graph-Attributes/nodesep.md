---
title: nodesep
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "nodesep", "double"]
date: 2018-04-15

description: "In dot, this specifies the minimum space between two adjacent nodes in the same rank, in inches."
types: ["double"]
default: "0.25"
min: "0.02"
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

In dot, this specifies the minimum space between two adjacent nodes in the same rank, in inches. For other layouts, this affects the spacing between loops on a single node, or multiedges between a pair of nodes.
