---
title: bb
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "bb", "rect"]
date: 2018-04-15

description: "Bounding box of drawing in points."
types: ["rect"]
default: "n/a"
min: ""
restrictions: "write only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Bounding box of drawing in points.
