---
title: pad
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "pad", "double", "point"]
date: 2018-04-15

description: "The pad attribute specifies how much, in inches, to extend the drawing area around the minimal area needed to draw the graph."
types: ["double", "point"]
default: "0.0555(double), 4 (points)"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

The pad attribute specifies how much, in inches, to extend the drawing area around the minimal area needed to draw the graph. If the pad is a single double, both the x and y pad values are set equal to the given value. This area is part of the drawing and will be filled with the background color, if appropriate.
