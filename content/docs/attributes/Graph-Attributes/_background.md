---
title: _background
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "_background", "string"]
date: 2018-04-15

description: "A string in the xdot format specifying an arbitrary background."
types: ["string"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

A string in the xdot format specifying an arbitrary background.
