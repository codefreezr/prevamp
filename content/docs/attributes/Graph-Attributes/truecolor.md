---
title: truecolor
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "truecolor", "bool"]
date: 2018-04-15

description: "If set explicitly to true or false, the value determines whether or not internal bitmap rendering relies on a truecolor color model or uses a color palette."
types: ["bool"]
default: "n/a"
min: ""
restrictions: "bitmap output only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If set explicitly to true or false, the value determines whether or not internal bitmap rendering relies on a truecolor color model or uses a color palette.
