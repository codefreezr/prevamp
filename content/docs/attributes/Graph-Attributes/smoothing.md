---
title: smoothing
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "smoothing", "smoothtype"]
date: 2018-04-15

description: "Specifies a post-processing step used to smooth out an uneven distribution of nodes."
types: ["smoothType"]
default: "n/a"
min: ""
restrictions: "sfdp only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Specifies a post-processing step used to smooth out an uneven distribution of nodes.
