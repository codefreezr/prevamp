---
title: Damping
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "damping", "double"]
date: 2018-04-15

description: "Factor damping force motions."
types: ["double"]
default: "0.99"
min: "0.0"
restrictions: "neato only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Factor damping force motions. On each iteration, a nodes movement is limited to this factor of its potential motion. By being less than 1.0, the system tends to 'cool', thereby preventing cycling.
