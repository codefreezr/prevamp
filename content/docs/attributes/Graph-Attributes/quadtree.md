---
title: quadtree
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "quadtree", "quadtype", "bool"]
date: 2018-04-15

description: "Quadtree scheme to use."
types: ["quadType", "bool"]
default: "normal"
min: ""
restrictions: "sfdp only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Quadtree scheme to use.
