---
title: repulsiveforce
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "repulsiveforce", "double"]
date: 2018-04-15

description: "The power of the repulsive force used in an extended Fruchterman-Reingold force directed model."
types: ["double"]
default: "1.0"
min: "0.0"
restrictions: "sfdp only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

The power of the repulsive force used in an extended Fruchterman-Reingold force directed model. Values larger than 1 tend to reduce the warping effect at the expense of less clustering.
