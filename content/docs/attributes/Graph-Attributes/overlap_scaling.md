---
title: overlap_scaling
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "overlap_scaling", "double"]
date: 2018-04-15

description: "When overlap=prism, the layout is scaled by this factor, thereby removing a fair amount of node overlap, and making node overlap removal faster and better able to retain the graph's shape."
types: ["double"]
default: "-4"
min: "-1.0e10"
restrictions: "prism only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

When overlap=prism, the layout is scaled by this factor, thereby removing a fair amount of node overlap, and making node overlap removal faster and better able to retain the graph's shape.
