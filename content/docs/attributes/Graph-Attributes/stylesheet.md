---
title: stylesheet
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "stylesheet", "string"]
date: 2018-04-15

description: "A URL or pathname specifying a css style sheet, used in SVG output."
types: ["string"]
default: "n/a"
min: ""
restrictions: "svg only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

A URL or pathname specifying a css style sheet, used in SVG output.
