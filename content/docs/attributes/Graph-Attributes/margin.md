---
title: margin
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "margin", "double", "point"]
date: 2018-04-15

description: "For graphs, this sets x and y margins of canvas, in inches."
types: ["double", "point"]
default: "n/a"
min: ""
restrictions: "device-dependent"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

For graphs, this sets x and y margins of canvas, in inches. If the margin is a single double, both margins are set equal to the given value.
