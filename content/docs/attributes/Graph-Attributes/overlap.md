---
title: overlap
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "overlap", "string", "bool"]
date: 2018-04-15

description: "Determines if and how node overlaps should be removed."
types: ["string", "bool"]
default: "TRUE"
min: ""
restrictions: "not dot"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Determines if and how node overlaps should be removed.
