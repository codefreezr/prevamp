---
title: defaultdist
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "defaultdist", "double"]
date: 2018-04-15

description: "This specifies the distance between nodes in separate connected components."
types: ["double"]
default: "1+(avg. len)*sqrt(|V|)"
min: "epsilon"
restrictions: "neato only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

This specifies the distance between nodes in separate connected components. If set too small, connected components may overlap. Only applicable if pack=false.
