---
title: normalize
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "normalize", "double", "bool"]
date: 2018-04-15

description: "If set, normalize coordinates of final layout so that the first point is at the origin, and then rotate the layout so that the angle of the first edge is specified by the value of normalize in degrees."
types: ["double", "bool"]
default: "FALSE"
min: ""
restrictions: "not dot"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If set, normalize coordinates of final layout so that the first point is at the origin, and then rotate the layout so that the angle of the first edge is specified by the value of normalize in degrees. If normalize is not a number, it is evaluated as a bool, with true corresponding to 0 degrees. NOTE: Since the attribute is evaluated first as a number, 0 and 1 cannot be used for false and true.
