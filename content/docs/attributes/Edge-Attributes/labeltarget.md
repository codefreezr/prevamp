---
title: labeltarget
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "labeltarget", "escstring"]
date: 2018-04-15

description: "If the edge has a URL or labelURL attribute, this attribute determines which window of the browser is used for the URL attached to the label."
types: ["escString"]
default: "n/a"
min: ""
restrictions: "svg, map only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If the edge has a URL or labelURL attribute, this attribute determines which window of the browser is used for the URL attached to the label. Setting it to '_graphviz' will open a new window if it doesn't already exist, or reuse it if it does. If undefined, the value of the target is used.
