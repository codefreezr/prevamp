---
title: labelangle
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "labelangle", "double"]
date: 2018-04-15

description: "This, along with labeldistance, determine where the headlabel (taillabel) are placed with respect to the head (tail) in polar coordinates."
types: ["double"]
default: "-25.0"
min: "-180.0"
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

This, along with labeldistance, determine where the headlabel (taillabel) are placed with respect to the head (tail) in polar coordinates. The origin in the coordinate system is the point where the edge touches the node. The ray of 0 degrees goes from the origin back along the edge, parallel to the edge at the origin. The angle, in degrees, specifies the rotation from the 0 degree ray, with positive angles moving counterclockwise and negative angles moving clockwise.
