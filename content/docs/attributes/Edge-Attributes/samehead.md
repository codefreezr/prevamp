---
title: samehead
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "samehead", "string"]
date: 2018-04-15

description: "Edges with the same head and the same samehead value are aimed at the same point on the head."
types: ["string"]
default: "n/a"
min: ""
restrictions: "dot only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Edges with the same head and the same samehead value are aimed at the same point on the head. This has no effect on loops. Each node can have at most 5 unique samehead values. See limitation.
