---
title: labelfloat
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "labelfloat", "bool"]
date: 2018-04-15

description: "If true, allows edge labels to be less constrained in position."
types: ["bool"]
default: "FALSE"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If true, allows edge labels to be less constrained in position. In particular, it may appear on top of other edges.
