---
title: edgetarget
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "edgetarget", "escstring"]
date: 2018-04-15

description: "If the edge has a URL or edgeURL attribute, this attribute determines which window of the browser is used for the URL attached to the non-label part of the edge."
types: ["escString"]
default: "n/a"
min: ""
restrictions: "svg, map only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If the edge has a URL or edgeURL attribute, this attribute determines which window of the browser is used for the URL attached to the non-label part of the edge. Setting it to '_graphviz' will open a new window if it doesn't already exist, or reuse it if it does. If undefined, the value of the target is used.
