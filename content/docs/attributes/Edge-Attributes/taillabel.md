---
title: taillabel
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "taillabel", "lblstring"]
date: 2018-04-15

description: "Text label to be placed near tail of edge."
types: ["lblString"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Text label to be placed near tail of edge. See limitation.
