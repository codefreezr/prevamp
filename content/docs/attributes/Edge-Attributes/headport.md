---
title: headport
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "headport", "portpos"]
date: 2018-04-15

description: "Indicates where on the head node to attach the head of the edge."
types: ["portPos"]
default: "center"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Indicates where on the head node to attach the head of the edge. In the default case, the edge is aimed towards the center of the node, and then clipped at the node boundary. See limitation.
