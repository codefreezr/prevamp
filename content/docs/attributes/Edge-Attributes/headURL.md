---
title: headURL
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "headurl", "escstring"]
date: 2018-04-15

description: "If headURL is defined, it is output as part of the head label of the edge."
types: ["escString"]
default: "n/a"
min: ""
restrictions: "svg, map only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If headURL is defined, it is output as part of the head label of the edge. Also, this value is used near the head node, overriding any URL value. See limitation.
