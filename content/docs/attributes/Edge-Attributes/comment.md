---
title: comment
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "comment", "string"]
date: 2018-04-15

description: "Comments are inserted into output."
types: ["string"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Comments are inserted into output. Device-dependent
