---
title: fontname
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "fontname", "string"]
date: 2018-04-15

description: "Font used for text."
types: ["string"]
default: "Times-Roman"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Font used for text.
