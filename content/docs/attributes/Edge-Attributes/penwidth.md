---
title: penwidth
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "penwidth", "double"]
date: 2018-04-15

description: "Specifies the width of the pen, in points, used to draw lines and curves, including the boundaries of edges."
types: ["double"]
default: "1.0"
min: "0.0"
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Specifies the width of the pen, in points, used to draw lines and curves, including the boundaries of edges. It has no effect on text.
