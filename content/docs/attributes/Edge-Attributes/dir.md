---
title: dir
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "dir", "dirtype"]
date: 2018-04-15

description: "Set edge type for drawing arrowheads."
types: ["dirType"]
default: "forward(directed), none(undirected)"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Set edge type for drawing arrowheads. This indicates which ends of the edge should be decorated with an arrowhead. The actual style of the arrowhead can be specified using the arrowhead and arrowtail attributes. See limitation.
