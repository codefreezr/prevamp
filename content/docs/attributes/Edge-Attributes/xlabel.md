---
title: xlabel
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "xlabel", "lblstring"]
date: 2018-04-15

description: "External label for an edge."
types: ["lblString"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

External label for an edge.
