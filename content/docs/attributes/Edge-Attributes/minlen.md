---
title: minlen
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "minlen", "int"]
date: 2018-04-15

description: "Minimum edge length (rank difference between head and tail)."
types: ["int"]
default: "1"
min: "0"
restrictions: "dot only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Minimum edge length (rank difference between head and tail).
