---
title: edgetooltip
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "edgetooltip", "escstring"]
date: 2018-04-15

description: "Tooltip annotation attached to the non-label part of an edge."
types: ["escString"]
default: "n/a"
min: ""
restrictions: "svg, cmap only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Tooltip annotation attached to the non-label part of an edge. This is used only if the edge has a URL or edgeURL attribute.
