---
title: weight
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "weight", "int", "double"]
date: 2018-04-15

description: "Weight of edge."
types: ["int", "double"]
default: "1"
min: "0(dot,twopi), 1(neato,fdp)"
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Weight of edge. In dot, the heavier the weight, the shorter, straighter and more vertical the edge is.
