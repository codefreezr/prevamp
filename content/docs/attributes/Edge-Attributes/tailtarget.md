---
title: tailtarget
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "tailtarget", "escstring"]
date: 2018-04-15

description: "If the edge has a tailURL, this attribute determines which window of the browser is used for the URL."
types: ["escString"]
default: "n/a"
min: ""
restrictions: "svg, map only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If the edge has a tailURL, this attribute determines which window of the browser is used for the URL. Setting it to '_graphviz' will open a new window if it doesn't already exist, or reuse it if it does. If undefined, the value of the target is used.
