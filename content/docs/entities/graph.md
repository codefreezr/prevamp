---
title: Graph
categories: ["entity"]
tags: ["tbd", "entity", "primary", "graph"]
tags-weight: 10
date: 2018-04-15

description: "GraphViz uses the DOT language to describe a graph."
lastEdit: 2018-04-15
lastEditor: generator
---

Shaping: digraph, graph

GraphViz uses the DOT language to describe a graph. There are a "digraph" for a directed graph and a "graph" for unidirected graphs.  

2do: List example empty graphs with styling heraders.  
2do: List all 1st class entities: defs, node, subgraph, rank, ...  
2do: Link to all graph attributes.
