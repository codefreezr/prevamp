---
title: Node
categories: ["entity"]
tags: ["tbd", "entity", "primary", "node"]
tags-weight: 20
date: 2018-04-15

description: "Node is a primary 1st class element in a graph."
lastEdit: 2018-04-15
lastEditor: generator
---

Shaping: polygon-based, record-based, user-defined

Node is a primary 1st class element in a graph. Shapes of nodes can be polygon-based, record-based or user-defined. They can be assorted into cluster, layer or ranks. Nodes can have label and style. See nodes-attributes.  

2do: Overview Shapes.  
2do: Link to Node Attributes
