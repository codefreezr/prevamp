---
title: Edge
categories: ["entity"]
tags: ["tbd", "entity", "primary", "edge"]
tags-weight: 30
date: 2018-04-15

description: "The connection between two nodes is an edge."
lastEdit: 2018-04-15
lastEditor: generator
---

Shaping: a -> b, a -- b

The connection between two nodes is an edge. An edge can be directed, for e.g. a -> b or non-directed, for e.e.g a -- b.  Edges can have arrows, labels, style.  

2do: Overview Arrows.  
2do: Link to Edge Attributes
