---
title: splineType
categories: ["Enum"]
tags: ["tbd", "splinetype", "enum"] 
date: 2018-04-15

description: "If a spline has points p1 p2 p3 pn, (n = 1 (mod 3)), the points correspond to the control points of a cubic B-spline from p1 to pn."
lastEdit: 2018-04-15
lastEditor: generator
---

If a spline has points p1 p2 p3 pn, (n = 1 (mod 3)), the points correspond to the control points of a cubic B-spline from p1 to pn. If startp is given, it touches one node of the edge, and the arrowhead goes from p1 to startp. If startp is not given, p1 touches a node. Similarly for pn and endp.
