---
title: escString
categories: ["String"]
tags: ["tbd", "escstring", "string"] 
date: 2018-04-15

description: "A string allowing escape sequences which are replaced according to the context."
lastEdit: 2018-04-15
lastEditor: generator
---

A string allowing escape sequences which are replaced according to the context.
