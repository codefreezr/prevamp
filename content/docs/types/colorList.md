---
title: colorList
categories: ["List of Strings"]
tags: ["tbd", "colorlist", "list of strings"] 
date: 2018-04-15

description: "A colon-separated list of weighted color values."
lastEdit: 2018-04-15
lastEditor: generator
---

A colon-separated list of weighted color values. WC(:WC)* where each WC has the form C(;F)? with C a color value and the optional F a floating-point number, 0 ? F ? 1. The sum of the floating-point numbers in a colorList must sum to at most 1.
