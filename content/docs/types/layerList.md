---
title: layerList
categories: ["List of Strings"]
tags: ["tbd", "layerlist", "list of strings"] 
date: 2018-04-15

description: "List of strings separated by characters from the layersep attribute (by default, colons, tabs or spaces), defining layer names and implicitly numbered 1,2,."
lastEdit: 2018-04-15
lastEditor: generator
---

List of strings separated by characters from the layersep attribute (by default, colons, tabs or spaces), defining layer names and implicitly numbered 1,2,...
