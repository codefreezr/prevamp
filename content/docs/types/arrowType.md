---
title: arrowType
categories: ["Enum"]
tags: ["tbd", "arrowtype", "enum"] 
date: 2018-04-15

description: "The basic arrows shown above contain most of the primitive shapes plus ones that can be derived from the o/inv grammar plus some supported as special cases for backward-compatibility."
lastEdit: 2018-04-15
lastEditor: generator
---

Values: (box, crow, diamond, dot, inv, none, normal, tee, vee)(odot, invdot, invodot, obox, odiamond) (ediamond, open, halfopen, empty, invempty)

The basic arrows shown above contain most of the primitive shapes plus ones that can be derived from the o/inv grammar plus some supported as special cases for backward-compatibility.
