---
title: rect
categories: ["Two Coordinates"]
tags: ["tbd", "rect", "two coordinates"] 
date: 2018-04-15

description: "The rectangle llx,lly,urx,ury."
lastEdit: 2018-04-15
lastEditor: generator
---

Values: %f,%f,%f,%f (llx,lly,urx,ury)

The rectangle llx,lly,urx,ury. It gives the coordinates, in points, of the lower-left corner (llx,lly) and the upper-right corner (urx,ury).
