---
title: startType
categories: ["Enum"]
tags: ["tbd", "starttype", "enum"] 
date: 2018-04-15

description: "If style is present, it must be one of the strings."
lastEdit: 2018-04-15
lastEditor: generator
---

Values:  regular, self or random

If style is present, it must be one of the strings.
